import { Component } from '@angular/core';
import { ExerciceModel } from '../models/exercice-model';
import { MatListModule } from '@angular/material/list';
import { DetailExerciceComponent } from '../detail-exercice/detail-exercice.component';

@Component({
  selector: 'app-gestion-exercices',
  standalone: true,
  imports: [MatListModule, DetailExerciceComponent],
  templateUrl: './gestion-exercices.component.html',
  styleUrl: './gestion-exercices.component.css',
})
export class GestionExercicesComponent {
  tabExercice: ExerciceModel[] = [
    {
      id: 1,
      nom: 'intro Typescript',
      dateDeRendu: new Date(2024, 9, 27),
      rendu: true,
    },
    {
      id: 2,
      nom: 'démarrage du joli gestionnaire de devoirs',
      dateDeRendu: new Date(2024, 11, 3),
      rendu: false,
    },
    {
      id: 3,
      nom: "ajout d'un routeur",
      dateDeRendu: new Date(2024, 12, 22),
      rendu: false,
    },
  ];

  selectedExercice: ExerciceModel | null = null;
  details(id: number) {
    this.selectedExercice = this.tabExercice[id - 1];
  }

  updateRendu(id: number) {
    let index = this.tabExercice.findIndex((item) => item.id == id);
    this.tabExercice[index].rendu = true;
  }

  updateRenduAnnule() {
    let index = this.tabExercice.findIndex(
      (item) => item.id == this.selectedExercice?.id
    );
    this.tabExercice[index].rendu = false;
  }
}
