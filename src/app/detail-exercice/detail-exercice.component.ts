import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  MatCardModule,
  MatCard,
  MatCardTitle,
  MatCardContent,
  MatCardActions,
} from '@angular/material/card';
import { MatCheckboxModule, MatCheckbox } from '@angular/material/checkbox';
import { MatButtonModule, MatButton } from '@angular/material/button';

import { DatePipe } from '@angular/common';
import { ExerciceModel } from '../models/exercice-model';

@Component({
  selector: 'app-detail-exercice',
  standalone: true,
  imports: [
    MatCardModule,
    MatCard,
    MatCardTitle,
    MatCardContent,
    MatCardActions,
    MatCheckbox,
    DatePipe,
    MatButtonModule,
    MatButton,
  ],
  templateUrl: './detail-exercice.component.html',
  styleUrl: './detail-exercice.component.css',
})
export class DetailExerciceComponent {
  @Input()
  exercice!: ExerciceModel | null;

  @Output()
  changeRendu = new EventEmitter<number>();

  @Output()
  annuleRenduEmit = new EventEmitter<number>();

  rendu(checked: boolean) {
    if (checked) {
      this.changeRendu.emit(this.exercice?.id);
    }
  }
  annuleRendu() {
    this.annuleRenduEmit.emit(this.exercice?.id);
  }
}
