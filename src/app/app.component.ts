import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { GestionExercicesComponent } from './gestion-exercices/gestion-exercices.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, GestionExercicesComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {
  title = 'Le mec la';
}
